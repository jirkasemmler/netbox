from datetime import datetime
from django.http import JsonResponse
from django.views import View

from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination

from data_synchronizer import Synchronizer
from serializers import *


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 20


class UpdateData(View):
    def get(self, request):
        synchronizer = Synchronizer()
        result = synchronizer.synchronize_data()
        return JsonResponse(result)


class ShowsView(viewsets.ReadOnlyModelViewSet):
    """
    view to return shows based on genre query or actor name query
    """

    def get_queryset(self):
        params = self.request.query_params
        genre_name = params.get('genre', None)
        actor_name = params.get('actor', None)
        filter_cr = {}
        if genre_name is not None:
            filter_cr['genres__name__startswith'] = genre_name
        if actor_name is not None:
            filter_cr['actors__name__startswith'] = actor_name
        return Show.objects.filter(**filter_cr)

    http_method_names = ['get']
    serializer_class = ShowSerializer
    pagination_class = StandardResultsSetPagination


class ShowsProgramView(viewsets.ReadOnlyModelViewSet):
    """
    view to return shows with it's datetime based on genre and datetime
    """

    def get_queryset(self):
        params = self.request.query_params
        genre_name = params.get('genre', None)
        date_param = params.get('date', None)
        time_param = params.get('time', None)

        if date_param is None or time_param is None:
            raise serializers.ValidationError('date and time are required parameters')

        filter_cr = {}
        if genre_name is not None:
            filter_cr['genres__name__startswith'] = genre_name
        try:
            datetime_obj = datetime.strptime(date_param + time_param, '%Y-%m-%d%H:%M')
            filter_cr['planning__starting_at__lt'] = datetime_obj
            filter_cr['planning__ending_at__gte'] = datetime_obj
        except ValueError:
            raise serializers.ValidationError('incorrect date or time')

        return Show.objects.filter(**filter_cr)

    http_method_names = ['get']
    serializer_class = ShowProgramSerializer
    pagination_class = StandardResultsSetPagination
