from .models import *
from rest_framework import serializers


class ShowSerializer(serializers.ModelSerializer):
    class Meta:
        model = Show
        fields = ('uid', 'name', 'original_name', 'short_desc', 'last_updated')


class ActorsSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source='person.name')

    class Meta:
        model = Character
        fields = ('name', 'role_name')


class ProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = Planning
        fields = ('starting_at', 'ending_at')


class ChannelSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.ReadOnlyField(source='channel.name')
    type = serializers.ReadOnlyField(source='channel.type')
    company = serializers.ReadOnlyField(source='channel.company_name')

    class Meta:
        model = Planning
        fields = ('name', 'type', 'company', 'starting_at', 'ending_at')


class ShowProgramSerializer(serializers.ModelSerializer):
    actors = ActorsSerializer(many=True, read_only=True, source='character_set')
    occurrences = ChannelSerializer(many=True, read_only=True, source='planning_set')
    genres = serializers.StringRelatedField(many=True)

    class Meta:
        model = Show
        fields = ('uid', 'name', 'original_name', 'short_desc', 'last_updated', 'actors', 'genres', 'occurrences')
