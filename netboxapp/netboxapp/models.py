from django.core.urlresolvers import reverse
from django.db import models as models


class Channel(models.Model):
    # Fields
    name = models.TextField(max_length=100)
    type = models.CharField(max_length=30)
    company_name = models.TextField(max_length=100)

    def __unicode__(self):
        return u'%s' % self.name


class Planning(models.Model):
    # Fields
    date = models.DateField()
    starting_at = models.DateTimeField(null=True)
    ending_at = models.DateTimeField(null=True)

    channel = models.ForeignKey(Channel, on_delete=models.CASCADE)
    show = models.ForeignKey('Show', on_delete=models.CASCADE)


class ShowImage(models.Model):
    file_name = models.CharField(max_length=255, unique=True)
    file_format = models.CharField(max_length=50)


class Genres(models.Model):
    # Fields
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return u'%s' % self.name


class Character(models.Model):
    # Fields
    role_name = models.CharField(max_length=100, null=True)
    person = models.ForeignKey('Person', on_delete=models.CASCADE)
    show = models.ForeignKey('Show', on_delete=models.CASCADE)

    directing = None
    acting = None
    producing = None


class Person(models.Model):
    # Fields
    name = models.CharField(max_length=255)

    def __unicode__(self):
        return u'%s' % self.name


class Show(models.Model):
    # Fields
    uid = models.IntegerField(unique=True, primary_key=True)
    name = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    original_name = models.TextField(max_length=255, null=True)
    show_length = models.IntegerField()
    short_desc = models.TextField(max_length=1000, null=True)
    long_desc = models.TextField(max_length=5000, null=True)

    # series
    is_series = models.NullBooleanField(default=False, null=True)
    season = models.IntegerField(default=None, null=True)
    episode = models.IntegerField(default=None, null=True)

    imdb_id = models.CharField(max_length=255, null=True)
    tmdb_id = models.IntegerField(null=True)

    imdb_rating = models.FloatField(null=True)

    # Relationship Fields
    occurrences = models.ManyToManyField(Channel, through='Planning')
    actors = models.ManyToManyField(Person, through=Character)
    producers = models.ManyToManyField(Person, related_name='producing')
    directors = models.ManyToManyField(Person, related_name='directing')
    scriptwriters = models.ManyToManyField(Person, related_name='scripting')
    genres = models.ManyToManyField(Genres, related_name='shows')
    cover_image = models.ForeignKey(ShowImage, on_delete=models.CASCADE, related_name='show_cover', null=True)
    landscape_image = models.ForeignKey(ShowImage, on_delete=models.CASCADE, related_name='show_landscape', null=True)
    main_image = models.ForeignKey(ShowImage, on_delete=models.CASCADE, related_name='show_main', null=True)

    def __unicode__(self):
        return u'%s' % self.name

    def update(self, values):
        for attr, value in values.items():
            setattr(self, attr, value)
