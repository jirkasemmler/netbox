from PIL import Image
from django.db import transaction
from imdb import IMDb
from resizeimage import resizeimage
from slugify import slugify
from StringIO import StringIO
import urllib
from zipfile import ZipFile
import pytz

from datetime import datetime, time, timedelta

import os
from lxml import etree
from django.conf import settings
from .models import *


class Synchronizer(object):
    def __init__(self):
        self.imdb = IMDb()

        # resetting mutable vars
        self.shows_created, self.shows_updated, self.persons_created, self.images_stored = [], [], [], []
        self.errors = []

    # list of updated or created lists for reports
    shows_created = []
    shows_updated = []
    persons_created = []
    images_stored = []

    # list of updated or created entities for current iteration of show
    tmp_shows_created = []
    tmp_shows_updated = []
    tmp_persons_created = []
    tmp_imgs_stored = []

    # shows with detected error while saving
    errors = []

    imdb = None

    @staticmethod
    def __get_data():
        """
        downloads data from external resource, unzips and returns content as parsed xml
        :return: ElementTree object
        """
        try:
            # downloads zip file
            data_file = urllib.urlopen(settings.DATA_URL)
            # unzip file
            zipfile = ZipFile(StringIO(data_file.read()))
        except:
            raise Exception('Input data are not available')
        if zipfile.filelist.__len__() != 1:
            raise Exception(
                'ZipFile expects one file only in the archive' + str(zipfile.filelist.__len__()) + 'provided')

        # parse file (XML) to parsed structure
        return etree.parse(zipfile.open(zipfile.filelist[0].filename))

    def __clean_logs(self):
        self.tmp_shows_created, self.tmp_shows_updated, self.tmp_persons_created, self.tmp_imgs_stored = [], [], [], []

    def __merge_logs(self):
        self.shows_created = self.shows_created + self.tmp_shows_created
        self.shows_updated = self.shows_updated + self.tmp_shows_updated
        self.persons_created = self.persons_created + self.tmp_persons_created
        self.images_stored = self.images_stored + self.tmp_imgs_stored

    def synchronize_data(self):
        """
        gets data from external source and stores them in db
        :return: dict - keys of stored/updated records
        """
        xml_data = self.__get_data()
        channel_obj = self.__get_channel(xml_data.getroot())
        program_days = xml_data.getroot().getchildren()

        # iterate over received data and write to db
        for program_day in program_days:
            date_obj = datetime.strptime(program_day.get('datum'), '%Y-%m-%d')
            show_nodes = program_day.getchildren()
            for show_node in show_nodes:  # store data of retrieved Shows and putting to program for selected date
                self.__clean_logs()
                try:
                    with transaction.atomic():
                        self.__put_show_in_program(channel_obj, date_obj, show_node)
                    self.__merge_logs()
                except Exception as e:
                    self.errors.append(
                        {'tag': show_node.tag, 'values': show_node.values, 'exception': e.message})
        # returning stat data about the operation
        return {'shows_updated': self.shows_updated, 'shows_created': self.shows_created,
                'images_stored': self.images_stored,
                'persons_created': self.persons_created, 'errors': self.errors}

    def __put_show_in_program(self, channel_obj, date_obj, show_node):
        """
        stores data about the show and puts it in the program for selected date
        :type channel_obj: Channel
        :type date_obj: datetime
        :type show_node: _Element
        """
        show_obj = self.__store_show(show_node)  # storing the show
        starting_at_time_string = show_node.find('cas-od').text

        try:
            starting_at = datetime.strptime(starting_at_time_string, '%H.%M')
        except ValueError:
            # detecting time after midnight, passing the date to next day
            split_time = starting_at_time_string.split('.')
            if int(split_time[0]) >= 24 and int(split_time[1]) < 60:
                starting_at = time((int(split_time[0]) - 24), int(split_time[1]))
                date_obj += timedelta(days=1)  # tomorrow
            else:
                raise Exception('Incorrect starting time')

        # putting show in the program
        starting_at_date = datetime(date_obj.year, date_obj.month, date_obj.day, starting_at.hour, starting_at.minute,
                                    0, 0, pytz.UTC)
        show_len = show_node.find('delka').text
        ending = starting_at_date + timedelta(minutes=int(show_len))
        Planning.objects.get_or_create(channel=channel_obj, date=date_obj, show=show_obj, starting_at=starting_at_date,
                                       ending_at=ending)

    def __store_show(self, show_node):
        # mapping between columns in model and xml representation. used in __extract_data_from_node
        xml_obj_mapping = {'name': 'nazev', 'original_name': 'nazev-originalni', 'show_length': 'delka',
                           'short_desc': 'kratkypopis', 'long_desc': 'dlouhypopis', 'imdb_id': 'IMDB_id',
                           'tmdb_id': 'tmdbId', 'season': 'rada', 'episode': 'dil', 'is_series': 'jeSerial'}
        show_data = self.__extract_data_from_node(show_node, xml_obj_mapping)
        show_data['uid'] = show_node.get('id')  # from attribute
        if show_data['uid'] is None:
            raise Exception('missing UID')
        show_data['is_series'] = True if show_data['is_series'] == 'true' else False  # boolean casting

        try:
            show = Show.objects.get(uid=show_data['uid'])
            show.update(show_data)
            # count modifications
            self.tmp_shows_updated.append(show.name)
        except Show.DoesNotExist:
            show = Show(**show_data)  # building object from data in show_data. show_data keys has to fit to the model
            self.tmp_shows_created.append(show.name)
        show.save()

        # adding persons
        self.__add_persons_to_show(show_node.find('rezie'), show.directors)
        self.__add_persons_to_show(show_node.find('scenar'), show.scriptwriters)
        self.__add_persons_to_show(show_node.find('produkce'), show.producers)
        self.__add_actors_to_show(show_node.find('hraji'), show)

        # adding genres
        self.__add_genres(show_node.find('genres'), show)

        # adding images
        self.__store_images(show_node, show)

        # adding extra data from imdb if the key exists
        if show_data['imdb_id']:
            self.__add_imdb_data(show)
        return show

    def __add_imdb_data(self, show_obj):
        """
        retrieves data from imdb and updates the show
        :type show_obj: Show
        """
        show = self.imdb.get_movie(show_obj.imdb_id[2:])  # parsing the imdb id tt<id>
        if show:
            show_obj.imdb_rating = show.data['rating']

    def __store_images(self, show_node, show_object):
        """
        :type show_object: Show
        """
        # slug made from title for SEO
        slug = str(show_object.uid) + "_" + slugify(show_object.name)

        # saving all the images to filesystem and to db
        show_object.main_image = self.__download_and_save_image(show_node.find('obrazek').text, slug + '_main')

        show_object.cover_image = self.__download_and_save_image(show_node.find('cover').text, slug + '_cover')

        show_object.main_image = self.__download_and_save_image(show_node.find('landscape').text, slug + '_landscape')

    def __download_and_save_image(self, url, slug):
        """
        downloads image defined by url, stores it in filesystem and returns ShowImage object if all good.
        Returns None if file doesn't exist
        :param url: url string
        :param slug: name of the final file without format
        :return: ShowImage | None
        """
        # download image and store it as tmp file
        path = os.path.dirname(os.path.abspath(__file__))
        img_directory = path + settings.STATIC_URL + 'show_images/'
        downloaded_img = urllib.urlretrieve(url)  # saving img as tmpfile
        file_tmp_path = downloaded_img[0]

        # detection of successfully downloaded file through its existence in /tmp
        if os.path.isfile(file_tmp_path) and os.access(file_tmp_path, os.W_OK) and os.access(file_tmp_path, os.R_OK):
            # detecting format and renaming image
            img = Image.open(file_tmp_path)
            img_file_path = img_directory + slug

            self.__resize(img, img_file_path + '_big', 400, 225)
            self.__resize(img, img_file_path + '_small', 270, 225)
            self.tmp_imgs_stored.append(slug)
            os.remove(file_tmp_path)  # cleaning
            return ShowImage.objects.get_or_create(file_name=slug, file_format=img.format)[0]
        return None

    @staticmethod
    def __resize(image_pointer, filename, width, height):
        # removing if old image exists. Image content can be updated
        if os.path.exists(filename + '.' + image_pointer.format):
            os.remove(filename + '.' + image_pointer.format)

        resized = resizeimage.resize_thumbnail(image_pointer, [width, height])
        resized.save(filename + '.' + image_pointer.format, image_pointer.format)

        resized2x = resizeimage.resize_thumbnail(image_pointer, [width * 2, height * 2])
        resized2x.save(filename + '@2x.' + image_pointer.format, image_pointer.format)

    def __add_persons_to_show(self, xml_node, show_field):
        """
        saves persons and adds to selected field of show
        :param xml_node - can be None if it doesn't exist in xml
        :type xml_node: _Element
        :param show_field: field of the Show object where the person should be attached to
        """
        if xml_node is not None:
            show_field.clear()  # clearing old relations because the new ones are about to be created
            for person_node in xml_node.getchildren():
                person_name = person_node.find('jmeno').text
                person_obj = self.__store_person(person_name)  # creating object of Person
                show_field.add(person_obj)

    def __add_actors_to_show(self, xml_node, show):
        """
        creates the actors and adds them to the show
        :type xml_node: _Element
        :type show: Show
        """
        if xml_node is not None:
            for person_node in xml_node.getchildren():
                person_name = person_node.find('jmeno').text
                person_obj = self.__store_person(person_name)

                character_node = person_node.find('role')
                role_name = character_node.text if character_node is not None else None
                Character.objects.get_or_create(person=person_obj, show=show, role_name=role_name)

    @staticmethod
    def __add_genres(xml_node, show):
        """
        stores new genres and attaches them to the show
        :type show: Show
        """
        if xml_node is not None:
            # removing relations to create new ones
            show.genres.clear()
            for genre_node in xml_node.getchildren():
                genre_name = genre_node.text
                genre_obj = Genres.objects.get_or_create(name=genre_name)
                show.genres.add(genre_obj[0])

    def __store_person(self, name):
        """
        stores object of person based on the name
        :param name: string
        :return: Person
        """
        person = Person.objects.get_or_create(name=name)
        self.tmp_persons_created.append(name)
        return person[0]

    @staticmethod
    def __extract_data_from_node(node, mapping):
        """
        based on mapping (dictionary in form <wantedKey>:<keyInXML>) it extracts data from XML node
        and returns the data in dict indexed by <wantedKey>
        :type node: _Element
        :type mapping: dict
        :return: dict
        """
        show_data = {}
        for show_key, xml_key in mapping.items():
            child_node = node.find(xml_key)  # finding the node based on <keyInXML>
            # extracting data if the node exists
            show_data[show_key] = child_node.text.strip() if child_node is not None and child_node.text else None
        return show_data

    @staticmethod
    def __get_channel(channel_node):
        """
        get channel object
        :param channel_node: Element
        :return: Channel
        """
        channel_name = channel_node.get('televize')
        channel_comp = channel_node.get('stanice')
        channel_type = channel_node.get('typ-stanice')
        try:
            channel = Channel.objects.get(name=channel_name, type=channel_type, company_name=channel_comp)
        except Channel.DoesNotExist:
            channel = Channel(name=channel_name, type=channel_type, company_name=channel_comp)
            channel.save()
        return channel
