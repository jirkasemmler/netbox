from unittest import TestCase

import mock
import os

from data_synchronizer import Synchronizer
import settings


class TestSynchronizer(TestCase):
    out = None

    def tearDown(self):
        self.clean_images()
        self.out = None

    @staticmethod
    def load_ext_file(mock_urllib, path='./fixtures/inputData.zip'):
        tmp = open(path)
        mock_urllib.return_value = tmp

    @mock.patch('netboxapp.data_synchronizer.urllib.urlopen')
    def test_loads_data(self, mock_urllib):
        self.load_ext_file(mock_urllib)
        synchronizer = Synchronizer()
        self.out = synchronizer.synchronize_data()

        self.assertEqual(self.out['shows_updated'].__len__(), 0)
        self.assertEqual(self.out['shows_created'].__len__(), 4)
        self.assertEqual(self.out['images_stored'].__len__(), 12)
        self.assertEqual(self.out['persons_created'].__len__(), 28)
        self.assertEqual(self.out['errors'].__len__(), 0)

    @mock.patch('netboxapp.data_synchronizer.urllib.urlopen')
    def test_loads_compromized_data(self, mock_urllib):
        self.load_ext_file(mock_urllib, './fixtures/inputWrongXMLData.zip')
        synchronizer = Synchronizer()
        self.out = synchronizer.synchronize_data()

        self.assertEqual(self.out['shows_updated'].__len__(), 0)
        self.assertEqual(self.out['shows_created'].__len__(), 0)
        self.assertEqual(self.out['images_stored'].__len__(), 0)
        self.assertEqual(self.out['persons_created'].__len__(), 0)
        self.assertEqual(self.out['errors'].__len__(), 4)

    def clean_images(self):
        if self.out is None:
            return
        # clean the images
        base = os.path.dirname(os.path.abspath(__file__)) + settings.STATIC_URL + 'show_images/'
        for img in self.out['images_stored']:
            os.remove(base + img + '_small.JPEG')
            os.remove(base + img + '_small@2x.JPEG')
            os.remove(base + img + '_big@2x.JPEG')
            os.remove(base + img + '_big.JPEG')

    @mock.patch('netboxapp.data_synchronizer.urllib')
    def test_not_a_zip(self, mock_urllib):
        self.load_ext_file(mock_urllib, './fixtures/notAZip')
        synchronizer = Synchronizer()

        self.assertRaises(Exception, synchronizer.synchronize_data)

    @mock.patch('netboxapp.data_synchronizer.urllib')
    def test_wrong_content_of_zip(self, mock_urllib):
        self.load_ext_file(mock_urllib, './fixtures/inputDataMoreFiles.zip')
        synchronizer = Synchronizer()

        self.assertRaises(Exception, synchronizer.synchronize_data)
