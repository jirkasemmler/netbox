from django.conf.urls import url, include
from rest_framework import routers

from api import *

router = routers.DefaultRouter()
router.register(r'shows', ShowsView, base_name='shows')
router.register(r'program', ShowsProgramView, base_name='shows_programs')

urlpatterns = [
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/update-data', UpdateData.as_view()),
]
